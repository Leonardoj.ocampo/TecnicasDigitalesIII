# Trabajo Práctico número 3:

## Consigna:
### Objetivos

Transmitir un archivo de 1M con el RealTerm, recibir el archivo con Qt y almacenarlo
al disco.
Utilizando una aplicación estándar para comparación, verificar que el archivo se transfirió
correctamente.

El contenido del archivo a transferir debe ser creado para seguir la siguiente trama:
0x1b Contador(1 byte) + info (22byte)
En total cada cadena tiene 4 byte.

Cuando generen el archivo el contador deberá ser incrementado por cada cadena.
Usted deberá entregar el informe del practico y el docente podrá pedir que replique la
experiencia.

### Cosas que hacer


